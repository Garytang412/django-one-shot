from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListListView(ListView):
    model = TodoList


class TodoListDetailView(DetailView):
    model = TodoList


class TodoListCreateView(CreateView):
    model = TodoList
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/todolist_update.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListDeleteView(DeleteView):
    model = TodoList
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = "__all__"
    template_name = "todo_items/todoitem_form.html"

    def get_success_url(self) -> str:
        return reverse_lazy(
            "todo_list_detail", kwargs={"pk": self.object.list.pk}
        )


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = "__all__"
    template_name = "todo_items/todoitem_update.html"

    def get_success_url(self) -> str:
        return reverse_lazy(
            "todo_list_detail", kwargs={"pk": self.object.list.pk}
        )
